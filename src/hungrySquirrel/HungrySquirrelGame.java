/*
 * Final project Java 1 programming
 */
package hungrySquirrel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Duy Nguyen
 */
public class HungrySquirrelGame {
    
    static final String CMD_MSG = "Enter a command:";
    static final String QUIT ="quit";
    static String HELP_MSG ="u = move up\nd = move down\n"
            + "r = move right\nl = move left\nh= help\nquit = exit game\n";
    
    
    static NutClass[] nuts = new NutClass[NutClass.TOTAL_NUTS];
    static SquirrelClass squirrel = new SquirrelClass();
    
    private static int peanutCount;
    private static int almondCount;
    private static int totalPossiblePoints;
    
                
    public static void main(String[] args){
        
        try{
            Maze.create("maze.txt");
            squirrel.create();
            AddNuts();
            Maze.display();
            
            
            String line;
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader userInput = new BufferedReader(isr);
            System.out.print(HELP_MSG);
            System.out.print(CMD_MSG);
        
            while((line = userInput.readLine()) != null){
               if(line.trim().equalsIgnoreCase(QUIT)){
                   break;
               }
               else if(line.trim().equalsIgnoreCase("help")){
                   System.out.print(HELP_MSG);
               }
               else{
                   parseCommand(line);
                   if(squirrel.getScore() >= totalPossiblePoints){
                       System.out.println("You have eaten all the nuts! Game over!");
                       break;
                   }
               }
               System.out.print(CMD_MSG);
            }
        }
        catch(IOException Ex){
            System.out.println(Ex.getMessage());
        }       
    }
    
    private static void AddNuts(){
        for(int i=0; i < NutClass.TOTAL_NUTS; i++){
            if(Math.random() <0.5){
                nuts[i] = new PeanutClass();
                peanutCount++;
                totalPossiblePoints = totalPossiblePoints + 
                        PeanutClass.NutritionalPoints;
            }
            else{
                nuts[i] = new AlmondClass();
                almondCount++;
                totalPossiblePoints = totalPossiblePoints + 
                        AlmondClass.NutritionalPoints;
            }            
            nuts[i].create();
        }
        System.out.println(peanutCount + " peanuts and " + almondCount 
                + " almonds created.");
        System.out.println("Total possible points = " + totalPossiblePoints);
        
    }
    
    private static void parseCommand(String inputCmd){
        switch(inputCmd.toLowerCase()){
            case "u":
                squirrel.moveUp();
                break;
                
            case "d":
                squirrel.moveDown();
                break;
                
            case "l":
                squirrel.moveLeft();
                break;
                
            case "r":
                squirrel.moveRight();
                break;
                
            default:
                System.out.println("Invalid command.");                
        }
    }
}
