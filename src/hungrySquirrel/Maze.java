/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hungrySquirrel;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author User
 */
public class Maze {
    
    static final int MAX_MAZE_ROW = 20;
    static final int MAX_MAZE_COL = 50;    
    
    static char entity[][] = new char[MAX_MAZE_ROW][MAX_MAZE_COL]; 
    
    SquirrelClass squirrel = new SquirrelClass();
    
                
    static void create(String filename) throws IOException{
        // Read in file
        FileReader fin = new FileReader(filename);
        BufferedReader br = new BufferedReader(fin);
        
        String line;
        int i = 0;
        
        while((line = br.readLine()) != null){
            entity[i] = line.toCharArray();            
            i++;
        }
        fin.close();
    };
    
    static void display(){
        for(int i = 0; i < MAX_MAZE_ROW; i++){
            System.out.println(new String(entity[i]));
        }
    }
    
    static boolean available(int row, int column){
        if(entity[row][column] == ' '){
            return true;
        }
        return false;        
    }
    
    static boolean placeEntity(int x, int y, char symbol){
        entity[x][y] = symbol;        
        return true;
    }
    
    static void clearEntity(int x, int y){
        entity[x][y] = ' '; 
    }
    
    static char getEntity(int x, int y){
        return entity[x][y];
    }
}
