/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hungrySquirrel;

/**
 *
 * @author User
 */
public class NutClass extends EntityClass{
    
    static final int TOTAL_NUTS = 5;
    static final int MAX_X = Maze.MAX_MAZE_ROW;
    static final int MAX_Y = Maze.MAX_MAZE_COL;
        
    @Override
    void create(){
        boolean created = false;
        while(created == false){
            int x = (int) ((Math.random() * MAX_X));            
            int y = (int) ((Math.random() * MAX_Y));
           
            if(Maze.available(x, y)){
                Maze.placeEntity(x, y, Symbol);
                this.X_position = x;
                this.Y_position = y;
                created = true;
            }
        }
    }
    
}
