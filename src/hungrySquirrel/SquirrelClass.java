/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hungrySquirrel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author User
 */
public class SquirrelClass extends EntityClass {
    
    private static final String INITIAL_POS = "Enter intial position "
            + "for the squirrel (x,y): ";
    
    private int score;
    
    SquirrelClass(){
        Symbol = '@';
    }
    
    void moveLeft(){
        processMove(X_position, Y_position -1 );
    }
    
    void moveRight(){
        processMove(X_position, Y_position + 1);
    }
    
    void moveUp(){
        processMove(X_position - 1, Y_position );
        
    }
    
    void moveDown(){
        processMove(X_position + 1, Y_position );
    }
    
    int getScore(){
        return score;
    }
    
    @Override
    void create(){
        try{
            String line;
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader userInput = new BufferedReader(isr);
            System.out.print(INITIAL_POS);
        
            while((line = userInput.readLine()) != null){               
               String[] tokens = line.split(",");
               if(placeSquirrel(line)){
                   break;
               }
               else{
                   System.out.println("Invalid entry.");
                   System.out.print(INITIAL_POS);
               }
            }
        }
        catch(IOException Ex){
            System.out.println(Ex.getMessage());
        }
        
    }
    
    
    private boolean placeSquirrel(String strInfo){
        String tokens[] = strInfo.split(",");
        if(tokens.length == 2){
           try{
               int x = Integer.parseInt(tokens[0].trim());
               int y = Integer.parseInt(tokens[1].trim());
               if (Maze.available(x,y)){
                   Maze.placeEntity(x, y, this.Symbol);
                   this.X_position = x;
                   this.Y_position = y;
                   return true;
               }
               else{
                   System.out.println("This space is not available");
               }
               
           }
           catch(NumberFormatException NEx){
               System.out.println(NEx.getMessage());
           }
           catch(ArrayIndexOutOfBoundsException OutofBoundsEx){
               System.out.println(OutofBoundsEx.getMessage() 
                       + " is out of bounds.");
               System.out.println("x must be between 0 and " + Maze.MAX_MAZE_ROW);
               System.out.println("y must be between 0 and " + Maze.MAX_MAZE_COL);               
           }
        }
        return false;
    }
    
    private void processMove(int x, int y){
        if(Maze.available(x, y)){
           updateSquirrelPosition(x,y);
           Maze.display();
        }
        else{
            switch (Maze.getEntity(x, y)){
                case 'P':                    
                    score += PeanutClass.NutritionalPoints;
                    updateSquirrelPosition(x,y);
                    Maze.display();
                    System.out.println("Yum, I ate a peanut!");
                    displayScore();
                    
                    break;
                    
                case 'A':
                    score += AlmondClass.NutritionalPoints;
                    updateSquirrelPosition(x,y);    
                    Maze.display();
                    System.out.println("Yum, I ate an Almond!");
                    displayScore();
                    break;
                    
                case '*':
                    System.out.println("Can't move there! Try again.");
                    break;
                    
                default:
                    System.out.println("Move Error");
            }
        }        
    }
    
     private boolean isNut(char symbol){
       if(symbol == 'A' || symbol == 'P'){
           return true;
       }
       return false;
    }
     
    private void updateSquirrelPosition(int x, int y){
        Maze.placeEntity(x, y, Symbol);
        Maze.clearEntity(this.X_position, this.Y_position);
        this.X_position = x;
        this.Y_position = y;
    }
    
    void displayScore(){
        System.out.println("Score: " + score);
    }
}
